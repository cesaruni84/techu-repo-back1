// Referecia al paquete express
var express = require('express');
var bodyParser = require('body-parser');

// Referencia al servidor de Node
var app = express();
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// Referencia al archivo users.json
var userFIle = require('./data/users.json');
var accountsFIle = require('./data/accounts.json');

// Constantes
//const PORT = 3000;
const PORT = process.env.PORT || 3000;

const URL_BASE = '/api-peru/v1/';

//Operaciòn GET - all users - collection
app.get(URL_BASE + 'users',
    function (request, response) {
        response.status(200)
        response.send(userFIle);
});


//Operaciòn GET - all users - collection
app.get(URL_BASE + 'users/:id/accounts',
    function (request, response) {
        response.status(200)
        response.send(accountsFIle);
});


//Operaciòn GET - all users - collection
app.get(URL_BASE + 'users/accounts',
    function (request, response) {
        response.status(200)
        response.send(accountsFIle);
});


//Operaciòn GET - by user for ID
/*app.get(URL_BASE + 'users/:id/:id2',
    function (request, response) {
        let id = request.params.id;
        let id2 = request.params.id2;
        console.log(id);
        console.log(id2);
        response.status(200)
        response.send(userFIle[id-1]);
});*/



//Operaciòn GET - by user for ID
app.get(URL_BASE + 'users/:id',
    function (request, response) {
        let id = request.params.id;
        let respuesta = (userFIle[id-1] == undefined) ? {"msg":"No existe"}: userFIle[id-1];
        response.status(200)
        response.send(respuesta);
});

//Operaciòn GET - by user for ID
app.get(URL_BASE + 'usersq',
    function (request, response) {
        let maxOcurries = request.query.max;
        let i = 0;
        let userFIle_response = [];
        userFIle.forEach(element => {
            if (i < maxOcurries) {
                // userFIle_response [i] = element;
                userFIle_response.push(element);
            }
            i++;

        });
        response.status(200)
        response.send(userFIle_response);
});
//Operaciòn POST - LOGIN for user
app.post(URL_BASE + 'login',
    function (request, response) {
        let user = request.body.email;
        let pass = request.body.password;
        for (us of userFIle) {
            if (us.email == user) {
                if (us.password == pass) {
                    us.logged = true;
                    writeUserDataToFile(userFIle);

                    console.log("Login correcto!");
                    let respuesta = {
                        mensaje: 'Login correcto.',
                        idUsuario: us.userID,
                        logged: us.logged           
                    };
                    response.send({respuesta});
                } else {
                    console.log("Login incorrecto.");
                    response.send({ "msg": "Login incorrecto." });
                }
            } else  {
                response.send({ "msg": "Login incorrecto." });
            }
        }
    });

//Operaciòn POST - LOGOUT users.json
app.post(URL_BASE + 'logout/:id',
    function (request, response) {
        // var userId = request.body.id;
        var userId = request.params.id;

        for (us of userFIle) {
            if (us.userID == userId) {
                if (us.logged) {
                    delete us.logged; // borramos propiedad 'logged'
                    writeUserDataToFile(userFIle);
                    console.log("Logout correcto!");
                    response.send({ "msg": "Logout correcto.", "idUsuario": us.userID });
                } else {
                    console.log("Logout incorrecto.");
                    response.send({ "msg": "Logout incorrecto." });
                }
            } else {
                response.send({ "msg": "Logout incorrecto." });
            }
        }
    });

function writeUserDataToFile(data) {
    const fs = require('fs');
    let jsonUserData = JSON.stringify(data);

    fs.writeFile("./data/users.json", jsonUserData, "utf8",
        function (err) { //función manejadora para gestionar errores de escritura
            if (err) {
                console.log(err);
            } else {
                console.log("Datos escritos en 'user.json'.");
            }
        })

}

//Operaciòn POST - by user for ID
app.post(URL_BASE + 'users',
    function (request, response) {
        let idGen = userFIle.length + 1;
        

        let newUser = {
            id: idGen,
            first_name: request.body.first_name,
            last_name: request.body.last_name,
            email: request.body.email,
            password: request.body.password
           };

        userFIle.push(newUser);

        let respuesta = {
            mensaje: 'Usuario creado correctamente',
            usuario: newUser,
            lista_usuarios: userFIle           
        };

        response.status(201)
        response.send(respuesta);
});

//Operaciòn PUT - by user for ID
app.put(URL_BASE + 'users',
    function (request, response) {
        let existUser = {
            id: request.body.id,
            first_name: request.body.first_name,
            last_name: request.body.last_name,
            email: request.body.email,
            password: request.body.password
           };

        indexRowUpdate = userFIle.findIndex((user => user.id == existUser.id));
        console.log(indexRowUpdate);
        // userFIle[indexRowUpdate].first_name = existUser.first_name;
        userFIle[indexRowUpdate] = existUser;

        let respuesta = {
            mensaje: 'Usuario actualizado correctamente',
            usuario: userFIle[indexRowUpdate],
            lista_usuarios: userFIle           
        };

        // let id = request.params.id;
        response.status(201)
        response.send(respuesta);
});



//Operaciòn DELETE - by user for ID
app.delete(URL_BASE + 'users/:id',
    function (request, response) {
        let index = request.params.id;
        let respuesta = 
            (userFIle[index-1] == undefined) ? 
                {"msg":"No existe"}:
            {"Usuario elminado" : (userFIle.splice(index-1,1))};

        response.status(200)
        response.send(respuesta);

});

// Servidor escucharà en la URL (servidor local) 
// http://localhost:3000/holamundo
app.listen(PORT, function (params) {
    console.log('Node JS escuchando en el puerto 3000 ....');
});


function random(low, high) {
    return Math.random() * (high - low) + low
  }