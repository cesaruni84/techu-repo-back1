// Referecia al paquete express
var express = require('express');
var bodyParser = require('body-parser');

// Referencia al servidor de Node
var app = express();
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// Referencia al archivo users.json
var userFIle = require('./users.json');

// Constantes
//const PORT = 3000;
const PORT = process.env.PORT || 3000;

const URL_BASE = '/api-peru/v1/';

//Operaciòn GET - all users - collection
app.get(URL_BASE + 'users',
    function (request, response) {
        response.status(200)
        response.send(userFIle);
});

//Operaciòn GET - by user for ID
/*app.get(URL_BASE + 'users/:id/:id2',
    function (request, response) {
        let id = request.params.id;
        let id2 = request.params.id2;
        console.log(id);
        console.log(id2);
        response.status(200)
        response.send(userFIle[id-1]);
});*/



//Operaciòn GET - by user for ID
app.get(URL_BASE + 'users/:id',
    function (request, response) {
        let id = request.params.id;
        let respuesta = (userFIle[id-1] == undefined) ? {"msg":"No existe"}: userFIle[id-1];
        response.status(200)
        response.send(respuesta);
});

//Operaciòn GET - by user for ID
app.get(URL_BASE + 'usersq',
    function (request, response) {
        let maxOcurries = request.query.max;
        let i = 0;
        let userFIle_response = [];
        userFIle.forEach(element => {
            if (i < maxOcurries) {
                // userFIle_response [i] = element;
                userFIle_response.push(element);
            }
            i++;

        });
        response.status(200)
        response.send(userFIle_response);
});


//Operaciòn POST - by user for ID
app.post(URL_BASE + 'users',
    function (request, response) {
        let idGen = userFIle.length + 1;
        

        let newUser = {
            id: idGen,
            first_name: request.body.first_name,
            last_name: request.body.last_name,
            email: request.body.email,
            password: request.body.password
           };

        userFIle.push(newUser);

        let respuesta = {
            mensaje: 'Usuario creado correctamente',
            usuario: newUser,
            lista_usuarios: userFIle           
        };

        response.status(201)
        response.send(respuesta);
});

//Operaciòn PUT - by user for ID
app.put(URL_BASE + 'users',
    function (request, response) {
        let existUser = {
            id: request.body.id,
            first_name: request.body.first_name,
            last_name: request.body.last_name,
            email: request.body.email,
            password: request.body.password
           };

        indexRowUpdate = userFIle.findIndex((user => user.id == existUser.id));
        console.log(indexRowUpdate);
        // userFIle[indexRowUpdate].first_name = existUser.first_name;
        userFIle[indexRowUpdate] = existUser;

        let respuesta = {
            mensaje: 'Usuario actualizado correctamente',
            usuario: userFIle[indexRowUpdate],
            lista_usuarios: userFIle           
        };

        // let id = request.params.id;
        response.status(201)
        response.send(respuesta);
});



//Operaciòn DELETE - by user for ID
app.delete(URL_BASE + 'users/:id',
    function (request, response) {
        let index = request.params.id;
        let respuesta = 
            (userFIle[index-1] == undefined) ? 
                {"msg":"No existe"}:
            {"Usuario elminado" : (userFIle.splice(index-1,1))};

        response.status(200)
        response.send(respuesta);

});

// Servidor escucharà en la URL (servidor local) 
// http://localhost:3000/holamundo
app.listen(PORT, function (params) {
    console.log('Node JS escuchando en el puerto 3000 ....');
});


function random(low, high) {
    return Math.random() * (high - low) + low
  }